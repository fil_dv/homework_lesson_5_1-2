﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_5_1_2
{
    public struct Article
    {
        public Article(ArticleType articleType, int id, string name, double price)
        {            
            _id = id;
            _name = name;
            _price = price;
            _articleType = articleType;
        }

        public ArticleType _articleType; 
        public int _id;
        public string _name;
        public double _price;
       
    }
}
