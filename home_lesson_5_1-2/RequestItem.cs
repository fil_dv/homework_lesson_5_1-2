﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_5_1_2
{
    public struct RequestItem
    {
        public RequestItem(Article foods, int numFoods)
        {
            _foods = foods;
            _numFoods = numFoods;
        }
        public Article _foods;
        public int _numFoods;
    }
}
