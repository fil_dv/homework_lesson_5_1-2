﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_5_1_2
{
    public enum ArticleType { drags, guns, chics }; 
    enum ClientType { vip, middle, looser};
    enum PayType { cash, card };   

    class Program
    {
        static void Main(string[] args)
        {
            Client cl = new Client(ClientType.vip, 1, "Djohn Doe", "LA Lenina str.", "3-22-33");
            Article ar1 = new Article(ArticleType.guns, 111, "gun", 1000);
            Article ar2 = new Article(ArticleType.drags, 222, "drags", 500);
            RequestItem ri1 = new RequestItem(ar1, 3);
            RequestItem ri2 = new RequestItem(ar2, 5);
            RequestItem[] arr = new RequestItem[2] { ri1, ri2 };
            Request r = new Request(PayType.cash, 1, cl, new DateTime(2012, 3, 8), arr);
            Console.WriteLine("Sum of request = {0}", r.SumOfRequest);  
        }
    }
}
