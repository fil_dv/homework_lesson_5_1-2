﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_5_1_2
{
    struct Request
    {
        public PayType _payType;
        public int _id;
        public Client _client;
        public DateTime _date;
        public RequestItem[] _requestItem;

        public Request(PayType payType, int id, Client client, DateTime date, RequestItem[] requestItem)
        {
            _payType = payType;
            _id = id;
            _client = client;
            _date = date;
            _requestItem = requestItem;            
        }

        public double SumOfRequest 
        {
            get
            {
                double res = 0;
                for (int i = 0; i < _requestItem.Length; ++i)
                {
                    res += _requestItem[i]._foods._price * _requestItem[i]._numFoods; 
                }
                return res;
            }
        }
    }
}
