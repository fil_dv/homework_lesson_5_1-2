﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_5_1_2
{
    struct Client
    {
        public Client(ClientType clientType, int id, string fio, string adress, string phone)
        {
            _clientType = clientType;
            _id = id;
            _fio = fio;
            _adress = adress;
            _phone = phone;
            _num = 0;
            _sum = 0;
        }

        public ClientType _clientType;
        public int _id;
        public string _fio;
        public string _adress;
        public string _phone;
        public int _num;
        public int _sum;
    }
}
